package Repositories;

import java.util.ArrayList;

public interface IRepositoryBase<E> {
	void Add(E o);
	ArrayList<E> ListAll();
	void Update(E o);
	void Delete(int id);
}
