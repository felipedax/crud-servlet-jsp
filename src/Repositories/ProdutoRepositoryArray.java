package Repositories;

import java.util.ArrayList;

import BD.ProdutosArrayList;
import Models.Produto;

public class ProdutoRepositoryArray implements IProdutoRepository {

	
	public ProdutoRepositoryArray() {
		super();
		/*
		Produto p1 = new Produto(1, "livro Servlet", 75.00);
		Produto p2 = new Produto(2, "caneta BIC", 0.70);
		Produto p3 = new Produto(3, "postit (15x)", 5.00);
		Produto p4 = new Produto(4, "mouse microsoft", 65.00);
		Produto p5 = new Produto(5, "escrivaninha simples", 55.00);
		dados.add(p1);
		dados.add(p2);
		dados.add(p3);
		dados.add(p4);
		dados.add(p5);
		*/
	}
	
	
	@Override
	public Produto Details(int id) {
		// TODO Auto-generated method stub
		
		for (Produto e : ProdutosArrayList.produtos) {
			if (e.getID() == id) {
				return e;
			}
		}
		return null;
	}

	@Override
	public void Add(Produto o) {
		// TODO Auto-generated method stub
		int proxIndex = ProdutosArrayList.produtos.size() +1;
		o.setID(proxIndex);
		ProdutosArrayList.produtos.add(o);
	}

	@Override
	public ArrayList<Produto> ListAll() {
		// TODO Auto-generated method stub
		return ProdutosArrayList.produtos;
	}

	@Override
	public void Update(Produto o) {
		// TODO Auto-generated method stub
		
		for (int i = 0; i < ProdutosArrayList.produtos.size(); i++) {
			
			if (ProdutosArrayList.produtos.get(i).getID() == o.getID()) {
				ProdutosArrayList.produtos.set(i, o);
			}
		}
	}

	@Override
	public void Delete(int id) {
		// TODO Auto-generated method stub
		for (int i = 0; i < ProdutosArrayList.produtos.size(); i++) {
			if (ProdutosArrayList.produtos.get(i).getID() == id) {
				ProdutosArrayList.produtos.remove(i);
				break;
			}
		}
	}
}
