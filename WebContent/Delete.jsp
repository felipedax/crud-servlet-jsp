<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Delete</title>
<%@ include file="Components/head.jsp" %>  
</head>
<body>
<%@ include file="Components/header.jsp" %>
<div class="container">
	<h1>DELETAR</h1>
	<form action="http://localhost:8080/CRUD/Delete" method = "post">
		<input class="form-control" value="${produto.getID()}" type="hidden" name="id">
		<div class="form-group">
		    <label>Nome</label>
		    ${produto.getNome()}
			<input class="form-control" value="${produto.getNome()}" type="hidden" name="nome">
		</div>
		<div class="form-group">
		    <label>Valor</label>
		    ${produto.getValor()}
			<input class="form-control" value="${produto.getValor()}" type="hidden" name="valor">
		</div>
		<input type="submit" class="btn btn-success">
	</form>
</div>
</body>
</html>